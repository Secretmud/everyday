# A course to learn Python

1. About the project
2. The game
3. PEP8
4. Code standards


## About the project

I didn't do any proper programming for almost a year(Except for my website).
This made me wanting to learn something new. So I picked up python, and what a
breeze it has been! Not only have I started to grasp the concept of OOP(Object
Oriented Programming). 

## The game

It's likely that the game will be abandoned at some point, for now it's just a
way of learning python and the various ways I can communicate with different
types of technology. 

## PEP8

I will try my hardest to follow the strict PEP8 standard as I learn. But I can't
promise anything. It might end up with me jumping into PEP8 when the project is
done deal.

## Code standards 

I'm a dick about the space/tab debate... I've yet to write a code stylesheet which
is to be followed if you want to contribute to the project(If that would even be a
possibility)

## FAQ

**Q**: What can I do with the source code?
**A**: Whatever you want aslong as you follow the license I use
**Q**: Do you do this for fun?
**A**: This pretty much sums up a perfect day.

## What's next?

I will be recreating the entire codebase, the reason for this is because I've
become alot better at python. This project was just to learn, and I've learnt
alot of new things!

## Contact

Send me a PM on [twitter](https://www.twitter.com/muderino) if you want to ask me something
