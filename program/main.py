#Text adventure written in python.
from packages.player import NewPlayer, Player
import os
import sys
import re
running = True


class Main():
    # Has the basic layout for the creation of a new
    # playable character
    def new_game(self):
        xp = 0
        level = 1
        gold = 100
        print("Welcome to the character selection!\nFirst of you need to pick a name!")
        name = input(str("What is your name?"))
        print(f"Nice to finaly meet you {name}.\nNow pick a class\n\n1. Mage\n2. Warrior\n\n")
        classType = input(str("Enter the name of the class you want to play\n"))
        if classType == '1':
            classType = 'Mage'
            print(f"Ok, {name}. I didn't know you played a {classType}\nWe also need your race!\nYou can pick one of the following:\n1. Human\n2. Orc")
        elif classType == '2':
            classType = 'Warrior'
            print(f"Ok, {name}. I didn't know you played a {classType}\nWe also need your race!\nYou can pick one of the following:\n1. Human\n2. Orc")
            
        race = input(str("Enter the name of the race you want to play\n"))
        if race == '1':
            race = 'Human'
        elif race == '2':
            race = 'Orc'
        print("Great, {name}! So you're a {classType} and you play a {race}!")
        NewPlayer.player_save(self, name, race, classType, xp, level, gold)
        os.system('clear')

    # The menu() function has the basic components of a 
    # menu. It will be worked upon at a later time, but 
    # for now it works perfectly
    def menu(self):
        print("This is the extremly simple and temporary menu:\n1. New game\n2. Load game\n3. Exit\n")
        choice = input("pick and item using numbers ranging from 1-3\n\n")
        if choice == '1':
            print("Nice you picked 1")
            os.system('clear')
            self.new_game()
        elif choice == '2':
            print("Do you want to lookup a previous save?\n")
            name = input("Write the name of your character:\n")
            if name == Player.load_player(self, name):
                print('Test')
            else:
                print(0)
            
        elif choice == '3':
            print("3 is my lucky number!\n\n\n Cya")
            sys.exit()
            
        else:
            print("Invalid")


m = Main()
while running:
    m.menu()
    
