#Everything related to the player will be here
import sqlite3



conn = sqlite3.connect('game.db')
c = conn.cursor()
class NewPlayer():
    def __init__(self, name, race, classType):
        self.name = name
        self.race = race
        self.classType = classType
    # the player_save function saves the player data
    # to the table
    def player_save(self, name, race, classType, xp, level, gold):
        Player.database_conn(self)
        c.execute("INSERT INTO player_data VALUES (?, ?, ?, ?, ?, ?)",(name, race, classType, xp, level, gold))
        conn.commit()
class Player():
    # the load player fucntions check if the player 
    # exists in the player_data table
    def load_player(self, name):
        t = (name,)
        c.execute('SELECT name FROM player_data WHERE name=?', t)
        print(c.fetchone())
    # the database_conn function is called if the 
    # the player has yet to play the game and the
    # software needs to create a table for the 
    # relevant player data
    def database_conn(self):
        c.execute('''CREATE TABLE IF NOT EXISTS player_data
             (name, race, classType, xp, level, gold)''')
    
