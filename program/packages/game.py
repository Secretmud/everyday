#All the actual game functionality will be here
from packages.monster import Monster
from packages.player import Player

class Game():
    # the town() function has a basic menu so that the
    # player can navigate throughout the game, this
    # menu will be more graphical at somepoint
    def town(self):
        print("Welcome to the town\n",
                "What do you want to do?\n",
                "1. go to the inn.\n",
                "2. go to the store\n",
                "3. go to the dungeon\n",
                "4. quit")

        choice = input("Enter the number of the action you want to perform\n\n")
    
        if choice == "1":
            self.inn()
        elif choice == "2":
            pass
        elif choice == "3":
            pass
        elif choice == "4":
            pass
        else:
            print("invalid option")
    # the inn() function has all I need for a basic inn
    def inn(self):
        print("Welcome to the inn",
                "What do you want to do?\n",
                "1. Sleep\n",
                "2. Eat\n",
                "3. Drink\n",
                "4. Leave\n")

        choice = input("Enter the number of the action you want to perform\n\n")

        if choice == "1":           
            pass                                       
        elif choice == "2":         
            pass                    
        elif choice == "3":         
            pass
        elif choice == "4":
            pass
        else:
            print("invalid option")

    
